// ACTIVITY - Quiz
/*
	1. What is the term given to unorganized code that's very hard to work with
		Ans. spaghetti code

	2. How are object literals written in JS?
		Ans. thru the use of {} with key-value pairs

	3. What do you call the concept of organizing information and  functionality to belong to an object?
		Ans. OOP

	4. If studentOne has a method named enroll(), how would you invoke it?
		Ans. studentOne.enroll();

	5. True or False: Objects can have objects as properties.
		Ans. True

	6. What is the syntax in creating key-value pairs?
		Ans. { key1: value1, key2: value2, ... }

	7. True or False: A method can have no parameters and still work.
		Ans. True

	8. True or False: Arrays can have objects as elements.
		Ans. True

	9. True or False: Arrays are objects.
		Ans. True

	10. True or False: Objects can have arrays as properties.
		Ans. True

*/

let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },

    // Mini-Exercise:
        // Create function/method that will get/compute the quarterly average of studentOne's grades

    computeAve() {
        let sum = 0; 
        this.grades.forEach(grade => sum = sum + grade);
        return sum/this.grades.length;
    },

    
    // Mini-Exercise 2:
        // Create function willPass() that will return true if average >= 85, and false if not.

    willPass(){
        return this.computeAve() >= 85;
    },


    // Mini-Exercise 3:
    // Create a function called willPassWithHonors() that returns true if the student has passed and their average grade is >= 90. The function returns false if either one is met.

    willPassWithHonors() {
        return (this.computeAve() >= 90) ? true 
        		: (this.computeAve() >= 85 && this.computeAve() < 90) ? false 
        		: undefined
    }
};

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// ACTIVITY - Function Coding
// FOR STUDENT TWO
// 1.
let studentTwo = {
    name: 'Joe',
    email: 'joe@mail.com',
    grades: [78, 82, 79, 85],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },

// 2.

    computeAve() {
        let sum = 0; 
        this.grades.forEach(grade => sum = sum + grade);
        return sum/this.grades.length;
    },

// 3.

    willPass(){
        return this.computeAve() >= 85;
    },


// 4.
    // Create a function called willPassWithHonors() that returns true if the average grade is >= 90, false if >=85 but <90, and undefined if <85

    willPassWithHonors() {
        return (this.computeAve() >= 90) ? true 
        		: (this.computeAve() >= 85 && this.computeAve() < 90) ? false 
        		: undefined
    }
};


// FOR STUDENT THREE
// 1.
let studentThree = {
    name: 'Jane',
    email: 'jane@mail.com',
    grades: [87, 89, 91, 93],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },

// 2.

    computeAve() {
        let sum = 0; 
        this.grades.forEach(grade => sum = sum + grade);
        return sum/this.grades.length;
    },

// 3.

    willPass(){
        return this.computeAve() >= 85;
    },


// 4.
    // Create a function called willPassWithHonors() that returns true if the average grade is >= 90, false if >=85 but <90, and undefined if <85

    willPassWithHonors() {
        return (this.computeAve() >= 90) ? true 
        		: (this.computeAve() >= 85 && this.computeAve() < 90) ? false 
        		: undefined
    }
};


// FOR STUDENT FOUR
// 1.
let studentFour = {
    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91, 89, 92, 93],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`)
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },

// 2.

    computeAve() {
        let sum = 0; 
        this.grades.forEach(grade => sum = sum + grade);
        return sum/this.grades.length;
    },

// 3.

    willPass(){
        return this.computeAve() >= 85;
    },


// 4.
    // Create a function called willPassWithHonors() that returns true if the average grade is >= 90, false if >=85 but <90, and undefined if <85

    willPassWithHonors() {
        return (this.computeAve() >= 90) ? true 
        		: (this.computeAve() >= 85) ? false 
        		: undefined
    }       
};
	

// 5.

let classof1A = {
    students: [studentOne, studentTwo, studentThree, studentFour],

// 6.

	countHonorStudents() {

        return (this.students.filter(student => student.willPassWithHonors() === true)).length;
    },


// 7.

    honorsPercentage() {

        let percentage = (this.countHonorStudents() / this.students.length) * 100;
        return percentage;
    },

// 8.

    retrieveHonorStudentInfo() {

        let honorStudents = [];

        this.students.forEach(student => {
            if (student.willPassWithHonors()) {
                honorStudents.push({
                	email: student.email,
                	aveGrade: student.computeAve()
            	});
            }
        });

        return honorStudents;
    },

// 9.

    sortHonorStudentsByGradeDesc() {

        let honorStudents = this.students.filter(student => student.willPassWithHonors());

        let sortedStudents = honorStudents.sort((a, b) => b.computeAve() - a.computeAve());

        let studentDetails = sortedStudents.map(student => {
            return {
                email: student.email,
                averageGrade: student.computeAve(),
            };
        });
        return studentDetails;
    }

};




